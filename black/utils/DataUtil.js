/**
 * 静态类
 */
import * as PIXI from '../libs/pixi.min'
window.DataUtil = class DataUtil{
	constructor(){
		throw new Error("DataUtil is a static class!can not create instance");
	}
}

DataUtil.getTimeStr = function(time){
    let min = Math.floor(time/60);
    let sec = time%60;
    return `剩余时间 ${min<10?'0'+min : min}:${sec<10?'0'+sec:sec}`;
}

//资源url前缀
DataUtil.baseResUrl = 'http://192.168.20.183:7676/';

/*通用白色字体*/
DataUtil.fontWhiteStyle = new PIXI.TextStyle({
    fill:"#ffffff",
    fontSize:36,
});
/*通用红色字体*/
DataUtil.fontRedStyle = new PIXI.TextStyle({
    fill:"#ff0000",
    fontSize:36,
});
/*通用绿色字体*/
DataUtil.fontGreenStyle = new PIXI.TextStyle({
    fill:"#00ff00",
    fontSize:36,
});
/*通用蓝色字体*/
DataUtil.fontBlueStyle = new PIXI.TextStyle({
    fill:"#0000ff",
    fontSize:36,
    align:'center',
});
/*通用标题字体*/
DataUtil.fontTitleStyle = new PIXI.TextStyle({
    fontSize:36,
    fill:"#FFD700",
    stroke:'#734A12',
    strokeThickness:5,
});
/*通用二级标题字体*/
DataUtil.fontTwoTitleStyle = new PIXI.TextStyle({
    fontSize:36,
    fill:"#FFD700",
    stroke:'#734A12',
    strokeThickness:3,
});
/*文字按钮style*/
DataUtil.fontBtnStyle = new PIXI.TextStyle({
	fontSize: 45,
	fontWeight: 'bold',
	fill: '#000000',
	strokeThickness:2,
	stroke: '#ffffff',
	letterSpacing:16,
});
/*通用特效字体style*/
DataUtil.fontEffStyle = new PIXI.TextStyle({
    fontSize: 64,
    fontStyle: 'italic',
    fontWeight: 'bold',
    fill: ['#ffff00', '#00ff00'], // gradient
    stroke: '#ff0099',
    strokeThickness: 3,
    dropShadow: true,
    dropShadowColor: '#000000',
    dropShadowBlur: 4,
    dropShadowAngle: Math.PI / 6,
    dropShadowDistance: 6,
    wordWrap: true,
    wordWrapWidth: 440
});
/**/
DataUtil.fontEff2Style = new PIXI.TextStyle({
    fontSize: 150,
    fontStyle: 'normal',
    fontWeight: 'bold',
    fill: ['#00ff00', '#555555'], // gradient
    stroke: '#ffff00',
    strokeThickness: 120,
    letterSpacing:20
});

/*过关提示style*/
DataUtil.fontPassStyle = new PIXI.TextStyle({
    fontSize: 64,
    fontStyle: 'italic',
    fontWeight: 'normal',
    fill: '#FFD700', // gradient
    stroke: '#FF6100',
    strokeThickness: 10,
});

/*chapter 1*/
DataUtil.fontResultStyle = new PIXI.TextStyle({
    fontSize: 48,
    fill: '#0000ff', // gradient
    stroke: '#ff0099',
    strokeThickness: 1,
    letterSpacing:8,
    leading:15,
    align:'center',
    wordWrap: true,
    wordWrapWidth: 550,
    breakWords:true,
});

//七彩颜色 type 1 - 7 默认0随机
DataUtil.getSevenColor = function(type = 0){
    type = type == 0 ? DataUtil.rndmnInt(1,7):type;
    let color = '';
    if(type == 1){
        color = 0xff0000;
    }
    else if(type == 2){
        color = 0xff7d00;
    }
    else if(type == 3){
        color = 0xffff00;
    }
    else if(type == 4){
        color = 0x00ff00;
    }
    else if(type == 5){
        color = 0x0000ff;
    }
    else if(type == 6){
        color = 0x00ffff;
    }
    else if(type == 7){
        color = 0xff00ff;
    }
    return color;
}

//概率 整数范围
DataUtil.rndmnInt = function(m,n){
    return Math.floor(m+Math.floor((n-m+1)* Math.random()));
}

//概率  非整数范围
DataUtil.rndmnFloat = function(m,n){
    return m+(n-m+1)* Math.random();
}