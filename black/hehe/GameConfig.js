//游戏配置类
let isNew = false;
export default class GameConfig{
	constructor(){
		if(isNew){
			throw new Error('GameEffect only have one instance');
		}
		isNew = true;
		this.init();
	}

	init(){
		// chapter:关卡类型  title:关卡名称  level：当前类型关卡的level  
		// 其他的参数 各类型关卡不一 主要如下
		// chapter1  content:随机字符串  result:结果字符串数组
		// chapter2  score:目标分数（达到即可过关）  speed：黑白快速度
		// chapter3  根据level增加障碍物的数量 后期可加
		// chapter4  score:目标分数 time：限制时间
		// chapter5  score:目标分数 time：限制时间
		// chapter6  根据level增加蜜蜂数量  time：蜜蜂间隔攻击时间 单位毫秒
		// chapter7  content：组成连连看的图像文字（同一个图像或文字必须是2的整数倍）  time：限制时间
		// chapter8  score:目标分数（达到即可过关）  speed:墙出现速度
		// chapter9  score:目标分数  times:失败机会次数
		// chapter10  score:目标分数  times:失败机会次数
		
		//only test
		this.levelConfig = [{
				chapter:10,
				level:1,
				score:10,
				title:'杀死比尔',
			}];
		return;

		this.levelConfig = [
			{
				chapter:1,
				level:1,
				time:2*60,
				title:'12生肖(按顺序)',
				content:`鼠牛虎兔龙蛇马羊猴鸡狗猪鸟猫象狮鹅鸭鱼驴龟鸽狼鹿熊鹰`,
				result:[`鼠牛虎兔龙蛇马羊猴鸡狗猪`],
			},
			{
				chapter:1,
				level:2,
				time:2*60,
				title:'组成无误的一句话',
				content:`黑人的一条黑狗被黑猫给黑了`,
				result:[`黑人的一条黑狗被黑猫给黑了`,
					`黑人的一条黑狗被黑给了黑猫`,
					`一条黑人的黑狗被黑猫给黑了`,
					`一条黑猫的黑狗被黑人给黑了`,
				],
			},
			{
				chapter:2,
				level:1,
				title:'别碰白块',
				score:50,
				speed:15
			},
			{
				chapter:2,
				level:2,
				title:'别碰白块',
				score:100,
				speed:20
			},
			{
				chapter:2,
				level:3,
				title:'别碰白块',
				score:200,
				speed:30
			},
			{
				chapter:3,
				level:1,
				title:'找到你'
			},
			{
				chapter:3,
				level:2,
				title:'找到你'
			},
			{
				chapter:3,
				level:3,
				title:'找到你'
			},
			{
				chapter:4,
				level:1,
				title:'乒乓球',
				score:50,
				time:120
			},
			{
				chapter:4,
				level:2,
				title:'乒乓球',
				score:100,
				time:120
			},
			{
				chapter:4,
				level:2,
				title:'乒乓球',
				score:200,
				time:150
			},
			{
				chapter:5,
				level:1,
				title:'打地鼠',
				score:50,
				time:60
			},
			{
				chapter:5,
				level:2,
				title:'打地鼠',
				score:100,
				time:60
			},
			{
				chapter:5,
				level:3,
				title:'打地鼠',
				score:200,
				time:90
			},
			{
				chapter:6,
				level:1,
				title:'小蜜蜂',
				time:500
			},
			{
				chapter:6,
				level:2,
				title:'小蜜蜂',
				time:400
			},
			{
				chapter:6,
				level:3,
				title:'小蜜蜂',
				time:300
			},
			{
				chapter:7,
				level:1,
				content:`ΔΔЖЖ※※☎☎☺☺☼☼♥♥☑☑♦♦❉❉℗℗☯☯❈❈✿✿♞♞♝♝✬✬☸☸☃☃♙♙♕♕☊☊☋☋❖❖⊕⊕¤¤▓▓♀♀♂♂鼴鼴ξξⅹⅹ∷∷∽∽╬╬☉☉◎◎ææɷɷʬʬѪѪѾѾѮѮ֎֎ᵺᵺ₴₴ŒŒ¥¥╳╳㐃㐃︽︽︾︾７７１１︰︰🐝🐝＆＆㘢㘢ㄖㄖポポ〥〥〓〓⑺⑺∵∵шшЗЗККΩΩ±±§§ΑΑ`,
				title:'简单连连看',
				time:240
			},
			{
				chapter:7,
				level:2,
				content:`ΔΔЖЖ※※☎☎☺☺☼☼♥♥☑☑♦♦❉❉℗℗☯☯❈❈✿✿♞♞♝♝✬✬☸☸☃☃♙♙♕♕☊☊☋☋❖❖⊕⊕¤¤▓▓♀♀♂♂鼴鼴ξξⅹⅹ∷∷∽∽╬╬☉☉◎◎ææɷɷʬʬѪѪѾѾѮѮ֎֎ᵺᵺ₴₴ŒŒ¥¥╳╳㐃㐃︽︽︾︾７７１１︰︰🐝🐝＆＆㘢㘢ㄖㄖポポ〥〥〓〓⑺⑺∵∵шшЗЗККΩΩ±±§§ΑΑ`,
				title:'简单连连看',
				time:270
			},
			{
				chapter:7,
				level:3,
				content:`ΔΔЖЖ※※☎☎☺☺☼☼♥♥☑☑♦♦❉❉℗℗☯☯❈❈✿✿♞♞♝♝✬✬☸☸☃☃♙♙♕♕☊☊☋☋❖❖⊕⊕¤¤▓▓♀♀♂♂鼴鼴ξξⅹⅹ∷∷∽∽╬╬☉☉◎◎ææɷɷʬʬѪѪѾѾѮѮ֎֎ᵺᵺ₴₴ŒŒ¥¥╳╳㐃㐃︽︽︾︾７７１１︰︰🐝🐝＆＆㘢㘢ㄖㄖポポ〥〥〓〓⑺⑺∵∵шшЗЗККΩΩ±±§§ΑΑ`,
				title:'简单连连看',
				time:300
			},
			{
				chapter:8,
				level:1,
				score:50,
				speed:5,
				title:'丑鸟先飞',
			},
			{
				chapter:9,
				level:1,
				score:1,
				times:10,
				title:'跳一跳',
			},
		]

	}
	//获取关卡配置
	getLevelConfig(level){
		return this.levelConfig[level - 1];
	}

}