import * as PIXI from '../../libs/pixi.min'
export default class BaseScene extends PIXI.Container{
	constructor(){
		super();
		this.isRunning = false;
	}

	enter(){
		if(this.isRunning) return;
		game.gameScene.addBottom(this);
		game.gameEffect.showInOut(this,Math.ceil(4*Math.random()));
		this.isRunning = true;
		game.app.ticker.add(this.update,this);
	}

	exit(){
		this.isRunning = false;
		if(this.parent){
			this.parent.removeChild(this);
		}
	}

	update(){
		if(!this.isRunning) return;
	}
}