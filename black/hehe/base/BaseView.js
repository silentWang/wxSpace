import * as PIXI from '../../libs/pixi.min'
export default class BaseView extends PIXI.Sprite{
	constructor(){
		super();
		this.isOpen = false;
	}

	open(){
		if(this.isOpen) return;
		this.isOpen = true;
		game.gameScene.addMiddle(this);
	}

	close(){
		this.isOpen = false;
		if(this.parent){
			this.parent.removeChild(this);
		}
	}
}