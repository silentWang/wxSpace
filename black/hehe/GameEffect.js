import * as PIXI from '../libs/pixi.min'

let isNew = false;
export default class GameEffect{
	constructor(){
		if(isNew){
			throw new Error('GameEffect only have one instance');
		}
		isNew = true;
	}
	//good excellent perfect and shit
	showPassEffect(callback,context,type = 0){
		let str = '';
		if(type == 0){
			str = 'good';
		}
		else if(type == 1){
			str = 'excellent';
		}
		else if(type == 2){
			str = 'perfect';
		}
		else if(type = -1){
			str = 'Oh shit!'
		}
		if(!this.passGrp){
			this.passGrp = new PIXI.Container();
			this.passTxt = new PIXI.Text(str,DataUtil.fontPassStyle);
			this.passTxt.style.fontWeight = 'bold';
			this.passTxt.anchor.set(0.5);
			this.passTxt.x = game.stageCenterX;
			this.passTxt.y = game.stageCenterY - 120;
			this.passGrp.addChild(this.passTxt);
		}
		if(this.passGrp.parent) return;
		this.passTxt.text = str;
		game.gameScene.addBottom(this.passGrp);
		this.passTxt.scale.set(10);
		let tween = PixiTween.scale(this.passTxt,2,2,30);
		tween.onComplete = ()=>{
			PixiTween.wait(1000).then(()=>{
				if(this.passGrp.parent){
					this.passGrp.parent.removeChild(this.passGrp);
				}
				if(callback && context){
					callback.call(context);
				}
				else if(callback){
					callback();
				}
			});
		};
	}

	//进出场
	showInOut(displayObject,type = 0){
		switch(type){
			case 0 : 
				displayObject.x = game.stageWidth;
				break;
			case 1 :
				displayObject.y = -game.stageHeight;
				break;
			case 2 :
				displayObject.y = game.stageHeight;
				break;
			case 3 :
				displayObject.x = game.stageWidth;
				displayObject.y = -game.stageHeight;
				break;
			case 4 :
				displayObject.x = game.stageWidth;
				displayObject.y = game.stageHeight;
				break;
			default:break;
		}
		PixiTween.slide(displayObject,0,0,20);
		
	}
}