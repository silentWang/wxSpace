import * as PIXI from '../libs/pixi.min'
import RankView from './view/RankView'
import MenuScene from './scene/MenuScene'
import OverScene from './scene/OverScene'
import Scene_01 from './scene/Scene_01'
import Scene_02 from './scene/Scene_02'
import Scene_03 from './scene/Scene_03'
import Scene_04 from './scene/Scene_04'
import Scene_05 from './scene/Scene_05'
import Scene_06 from './scene/Scene_06'
import Scene_07 from './scene/Scene_07'
import Scene_08 from './scene/Scene_08'
import Scene_09 from './scene/Scene_09'
import Scene_10 from './scene/Scene_10'
/*
*灵感
*文字组合(十二生肖)
*黑白块
*寻找你
*乒乓球
*打地鼠
*小蜜蜂
*连连看
*俄罗斯方块
*接鸡蛋
*打耙
*下100层
*
 */
export default class GameScene{
	constructor(){
		this.init();
	}

	init(){
		this.bottomLayer = new PIXI.Container();
		this.middleLayer = new PIXI.Container();
		this.topLayer = new PIXI.Container();
		game.app.stage.addChild(this.bottomLayer);
		game.app.stage.addChild(this.middleLayer);
		game.app.stage.addChild(this.topLayer);

		/*ui*/
		this.rankView = new RankView();
		/*scene*/
		this.sceneMap = new Map();
		this.sceneMap.set('menu',MenuScene);
		this.sceneMap.set('over',OverScene);
		//文字组合
		this.sceneMap.set('scene_01',Scene_01);
		//黑白块
		this.sceneMap.set('scene_02',Scene_02);
		//寻找
		this.sceneMap.set('scene_03',Scene_03);
		//乒乓球
		this.sceneMap.set('scene_04',Scene_04);
		//打地鼠
		this.sceneMap.set('scene_05',Scene_05);
		//小蜜蜂
		this.sceneMap.set('scene_06',Scene_06);
		//连连看
		this.sceneMap.set('scene_07',Scene_07);
		//胖鸟先飞
		this.sceneMap.set('scene_08',Scene_08);
		//跳一跳2D
		this.sceneMap.set('scene_09',Scene_09);
		this.sceneMap.set('scene_10',Scene_10);

		this.currentScene = null;
		this.currentLevel = 1;
	}

	gotoNextLevel(){
		this.currentLevel++;
		let config = game.gameConfig.getLevelConfig(this.currentLevel);
		this.enterScene(`scene_${config.chapter < 10 ? '0'+config.chapter : config.chapter}`);
	}

	getComnatResult(){
		return `你的战绩：第${this.currentLevel}关`;
	}

	enterScene(key){
		if(this.currentScene){
			this.currentScene.exit();
		}
		let config = game.gameConfig.getLevelConfig(this.currentLevel);
		this.currentScene = new (this.sceneMap.get(key))(config);
		this.currentScene.enter();
	}

	gameOver(){
		game.gameEffect.showPassEffect(()=>{
			this.enterScene('over');
		},this,-1);
	}

	addBottom(ui){
		if(!ui) throw new Error("must have a argument");
		this.bottomLayer.addChild(ui);
	}
	addMiddle(ui){
		if(!ui) throw new Error("must have a argument");
		this.middleLayer.addChild(ui);
	}
	addTop(ui){
		if(!ui) throw new Error("must have a argument");
		this.topLayer.addChild(ui);
	}
}