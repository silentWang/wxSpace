import * as PIXI from '../../libs/pixi.min'

export default class Button extends PIXI.Sprite{
	constructor(label = '',type = 0,width = 260,height = 100){
		super();
		this._label = label;
		this.bWidth = width;
		this.bHeight = height;
		this._type = type;
		this.init();
	}

	init(){
		this.interactive = true;
		this.buttonMode = true;
		let radius = Math.sqrt((this.bHeight/2)*(this.bHeight/2));
		let graphics = new PIXI.Graphics();
		graphics.beginFill(this.getColor());
		graphics.lineStyle(0,0xffffff,1);
		graphics.drawRoundedRect(0,0,this.bWidth,this.bHeight,radius);
		graphics.endFill();
		this.labeltxt = new PIXI.Text(this._label,DataUtil.fontBtnStyle);
		this.labeltxt.x = (this.bWidth - this.labeltxt.width)/2;
		this.labeltxt.y = (this.bHeight - this.labeltxt.height)/2;
		this.addChild(graphics);
		this.addChild(this.labeltxt);
	}

	/*1-7 红橙黄绿青蓝紫 默认green*/
	getColor(){
		let color = 0x00ff00;
		if(this._type == 1){
			color = 0xff0000;
		}
		else if(this._type == 2){
			color = 0xff7d00;
		}
		else if(this._type == 3){
			color = 0xffff00;
		}
		else if(this._type == 4){
			color = 0x00ff00;
		}
		else if(this._type == 5){
			color = 0x0000ff;
		}
		else if(this._type == 6){
			color = 0x00ffff;
		}
		else if(this._type == 7){
			color = 0xff00ff;
		}
		return color;
	}

	set label(value){
		this.labeltxt.text = value;
		this.labeltxt.x = (this.bWidth - this.labeltxt.width)/2;
		this.labeltxt.y = (this.bHeight - this.labeltxt.height)/2;
	}
	/*覆盖父类的width*/
	get width(){
		return this.bWidth;
	}
	get height(){
		return this.bHeight;
	}
}