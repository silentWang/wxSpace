import * as PIXI from '../../libs/pixi.min'
import BaseScene from '../base/BaseScene'
export default class Scene_03 extends BaseScene{
	constructor(data){
		super();
		this.data = data;//{level:1};
		this.init();
	}

	init(){
		this.ballsGrp = new PIXI.Container();
		this.addChild(this.ballsGrp);
		this.createBalls();
	}

	enter(){
		super.enter();
		this.player = new PIXI.Graphics();
		this.player.beginFill(0xffffff,0.5);
		this.player.lineStyle(2,0x00ff00,1);
		this.player.drawRect(0,0,60,60);
		this.player.endFill();
		let text = new PIXI.Text('我',{fill:'#000000',fontSize:32,fontWeight:'bold'});
		text.x = 30 - text.width/2;
		text.y = 30 - text.height/2;
		this.player.addChild(text);
		this.addChild(this.player);
		this.player.interactive = true;
		this.player.on("pointerdown",this.dragStart,this)
			.on("pointerup",this.dragEnd,this)
			.on("pointerupoutside",this.dragEnd,this)
			.on("pointermove",this.dragMove,this);

		this.target = new PIXI.Graphics();
		this.target.beginFill(0xffffff,0.5);
		this.target.lineStyle(2,0x00ff00,1);
		this.target.drawRect(0,0,60,60);
		this.target.endFill();
		this.target.x = game.stageWidth - 60;
		this.target.y = game.stageHeight - 60;
		let text2 = new PIXI.Text('你',{fill:'#000000',fontSize:32,fontWeight:'bold'});
		text2.x = 30 - text2.width/2;
		text2.y = 30 - text2.height/2;
		this.target.addChild(text2);
		this.addChild(this.target);
	}

	createBalls(){
		let len = this.getByLevel();
		for(let i = 0;i<len;i++){
			let graphics = new PIXI.Graphics();
			graphics.beginFill(0xff0000);
			graphics.lineStyle(2,0x00ff00,1);
			graphics.drawCircle(30,30,30);
			graphics.endFill();
			graphics.x = (game.stageWidth - 50)*Math.random();
			graphics.y = (game.stageHeight - 50)*Math.random();
			graphics.speedx = 2*(Math.random() + 1);
			graphics.speedy = 2*(Math.random() + 1);
			this.ballsGrp.addChild(graphics);
		}
	}

	getByLevel(){
		switch(this.data.level){
			case 1 : return 30;
			case 2 : return 50;
			case 3 : return 80;
			default:return 30;
		}
	}

	dragStart(event){
		this.dragData = event.data;
		this.isDragging = true;
		this.doffsety = this.player.y - this.dragData.global.y;
		this.doffsetx = this.player.x - this.dragData.global.x;
	}

	dragEnd(){
		this.isDragging = false;
		this.dragData = null;
		if(this.player.x < 0){
			this.player.x = 0;
		}
		if(this.player.x > game.stageWidth - 60){
			this.player.x = game.stageWidth - 60;
		}
		if(this.player.y < 0){
			this.player.y = 0;
		}
		if(this.player.y > game.stageHeight - 60){
			this.player.y = game.stageHeight - 60;
		}
	}

	dragMove(){
		if(this.isDragging){
			var newPos = this.dragData.getLocalPosition(this.player.parent);
			this.player.y = newPos.y + this.doffsety;
			this.player.x = newPos.x + this.doffsetx;
			if(this.player.x < 0){
				this.player.x = 0;
			}
			if(this.player.x > game.stageWidth - 60){
				this.player.x = game.stageWidth - 60;
			}
			if(this.player.y < 0){
				this.player.y = 0;
			}
			if(this.player.y > game.stageHeight - 60){
				this.player.y = game.stageHeight - 60;
			}
			if(PixiBump.hit(this.player,this.target)){
				this.isRunning = false;
				this.player.interactive = false;
				game.gameEffect.showPassEffect(()=>{
					game.gameScene.gotoNextLevel();
				});
			}
		}
	}


	update(){
		if(!this.isRunning) return;
		for(let ball of this.ballsGrp.children){
			ball.x += ball.speedx;
			ball.y += ball.speedy;
			if(ball.x > (game.stageWidth - 50)){
				ball.speedx *= -1;
			}
			else if(ball.x <= 0){
				ball.speedx *= -1;
			}
			if(ball.y > (game.stageHeight - 50)){
				ball.speedy *= -1; 
			}
			else if(ball.y < 0){
				ball.speedy *= -1;
			}
			if(PixiBump.hit(this.player,ball)){
				this.isRunning = false;
				this.player.interactive = false;
				game.gameScene.gameOver();
				break;
			}
		}
	}
	
}