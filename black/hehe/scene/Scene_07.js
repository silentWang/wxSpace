import * as PIXI from '../../libs/pixi.min'
import BaseScene from '../base/BaseScene'

export default class Scene_07 extends BaseScene{
	constructor(data){
		super();
		//content 元素  level 1,2,3 分别代表10*10 10*12 10*15  time:限制时间
		this.data = data;//{content:DataUtil.chapter7,level:1,time:300};
		this.init();
	}

	init(){
		this.score = 0;
		this.levelText = new PIXI.Text(`简单连连看`,DataUtil.fontWhiteStyle);
		this.levelText.x = 20;
		this.levelText.y = 20;

		this.timeLeft = new PIXI.Text(DataUtil.getTimeStr(this.data.time),DataUtil.fontGreenStyle);
		this.timeLeft.x = game.stageWidth - this.timeLeft.width - 20;
		this.timeLeft.y = 20;
		this.addChild(this.timeLeft);
		this.leftTime = this.data.time;
		this.addChild(this.levelText);

		this.curSelectItem = null;
		this.createList();
	}

	createList(){
		this.listGrp = new PIXI.Container();
		this.addChild(this.listGrp);
		let len = this.getByLevel();
		this.listGrp.x = 45;
		this.listGrp.y = (game.stageHeight - (len/10)*60)/2;

		this.listData = Array.from(this.data.content);
		this.listData = this.listData.slice(0,len);
		this.listData.sort(()=>{return 0.5-Math.random()});
		
		for(let i = 0,j=0;i<len;i++){
			let str = this.listData[i];
			if(!str){
				str = this.listData[j];
				j++;
			}
			let item = new Item(str);
			item.x = 64*(i%10);
			item.y = 64*Math.floor(i/10);
			this.listGrp.addChild(item);
			item.on('pointerdown',this.clkItem,this);
		}
	}

	getByLevel(){
		switch(this.data.level){
			case 1 : return 10*10;
			case 2 : return 10*12;
			case 3 : return 10*15;
			default:return 10*10;
		}
	}

	clkItem(e){
		let item = e.target;
		if(this.curSelectItem == null || item == this.curSelectItem){
			item.selected = !item.selected;
			this.curSelectItem = item.selected ? item:null;
		}
		else{
			if(this.curSelectItem.getID() == item.getID()){
				this.listGrp.removeChild(this.curSelectItem);
				this.listGrp.removeChild(item);
				this.curSelectItem = null;
				if(this.listGrp.children.length == 0){
					game.gameEffect.showPassEffect(()=>{
						game.gameScene.gotoNextLevel();
					},this);
				}
			}
			else{
				item.selected = true;
				this.curSelectItem.selected = false;
				this.curSelectItem = item;
			}
		}
	}

	enter(){
		super.enter();
		this.invalId = setInterval(()=>{
			this.leftTime--;
			this.timeLeft.text = DataUtil.getTimeStr(this.leftTime);
			if(this.leftTime <= 0){
				this.isRunning = false;
				game.gameScene.gameOver();
				clearInterval(this.invalId);
			}
		},1000);
	}

	exit(){
		super.exit();
		clearInterval(this.invalId);
	}

}

class Item extends PIXI.Sprite{
	constructor(idx){
		super();
		this.idx = idx;
		this.init();
	}

	init(){
		this.graphics = new PIXI.Graphics();
		this.graphics.lineStyle(2,0xA3A3A3,1);
		this.graphics.beginFill(0xFFBBFF);
		this.graphics.drawRect(0,0,62,62);
		this.graphics.endFill();
		this.addChild(this.graphics);
		let txt = new PIXI.Text(this.idx,{fontSize:48,fill:'#000000',stroke:'#0000ff',strokeThickness:1});
		txt.anchor.set(0.5);
		txt.x = 30;
		txt.y = 30;
		this.addChild(txt);

		this._selected = false;
		this.interactive = true;
		this.buttonMode = true;
	}

	getID(){
		return this.idx;
	}

	set selected(value){
		this._selected = value;
		if(this._selected){
			this.graphics.lineStyle(2,0x00ff00,1);
		}
		else{
			this.graphics.lineStyle(2,0xA3A3A3,1);
		}

		this.graphics.beginFill(0xFFBBFF);
		this.graphics.drawRect(0,0,62,62);
		this.graphics.endFill();
	}

	get selected(){
		return this._selected;
	}

}