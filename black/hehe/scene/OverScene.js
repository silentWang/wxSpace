import * as PIXI from '../../libs/pixi.min'
import Button from '../items/Button'
import BaseScene from '../base/BaseScene'

export default class OverScene extends BaseScene{
	constructor(){
		super();
		this.init();
	}

	init(){
		game.app.renderer.backgroundColor = 0xCDB7B5;
		let title = new PIXI.Text('Game Over',DataUtil.fontRedStyle);
		title.style = new PIXI.TextStyle({
			fill:0x6C7B8B,
			fontSize:120,
			fontWeight:'bold',
			stroke:0x363636,
			strokeThickness:20,
		});
		title.anchor.set(0.5);
		title.x = game.stageCenterX;
		title.y = game.stageCenterY - 260;
		this.addChild(title);

		this.scoreTxt = new PIXI.Text('你的战绩：第五关',DataUtil.fontBlueStyle);
		this.scoreTxt.anchor.set(0.5);
		this.scoreTxt.x = game.stageCenterX;
		this.scoreTxt.y = game.stageCenterY;
		this.addChild(this.scoreTxt);

		let button = new Button('主菜单',2);
		button.x = game.stageCenterX - button.width/2;
		button.y = this.scoreTxt.y + 80;
		this.addChild(button);
		button.on('pointerdown',()=>{
			this.exit();
			game.gameScene.enterScene('menu');
		});
	}

	enter(){
		super.enter();
		this.scoreTxt.text = game.gameScene.getComnatResult();
	}

}