import * as PIXI from '../../libs/pixi.min'
import Charm from '../../libs/Charm'
import BaseScene from '../base/BaseScene'
export default class Scene_01 extends BaseScene{
	constructor(data){
		super();
		this.data = data;//{level:1,content:DataUtil.chapter1[0],time:60}
		this.init();
	}

	init(){
		game.app.renderer.backgroundColor = 0x000000;
		this.gGrp = new PIXI.Container();
		this.addChild(this.gGrp);
		this.initTitle();
		this.initBottom();
		this.hasSelArr = [];
		this.isTweening = false;
	}

	initTitle(){
		this.tGrp = new PIXI.Container();
		this.addChild(this.tGrp);

		this.levelText = new PIXI.Text('第一关  组合文字',DataUtil.fontWhiteStyle);
		this.levelText.x = 30;
		this.levelText.y = 20;
		this.tGrp.addChild(this.levelText);

		this.timeLeft = new PIXI.Text('剩余时间 00:00',DataUtil.fontGreenStyle);
		this.timeLeft.x = game.stageWidth - this.timeLeft.width - 20;
		this.timeLeft.y = 20;
		this.tGrp.addChild(this.timeLeft);
		this.leftTime = this.data.time;
	}

	initBottom(){
		this.rGrp = new PIXI.Container();
		this.rGrp.y = 3/4*game.stageHeight;
		this.addChild(this.rGrp);
		let graphics = new PIXI.Graphics();
		graphics.lineStyle(10,0xffffff,1);
		graphics.beginFill(0x8EE5EE);
		graphics.moveTo(0,0);
		graphics.lineTo(game.stageWidth,0);
		graphics.moveTo(0,20);
		graphics.drawRect(0,0,game.stageWidth,game.stageHeight/4);
		graphics.endFill();
		this.rGrp.addChild(graphics);

		this.resultStr = '※';
		this.text = new PIXI.Text(this.resultStr,DataUtil.fontResultStyle);
		this.text.x = game.stageCenterX;
		this.text.y = 150;
		this.text.anchor.set(0.5);
		this.text.interactive = true;
		this.text.on('pointerdown',this.clkResult,this);
		this.rGrp.addChild(this.text);
	}

	enter(){
		super.enter();
		this.createAll();
		this.invalId = setInterval(()=>{
			this.leftTime--;
			this.timeLeft.text = DataUtil.getTimeStr(this.leftTime);
			if(this.leftTime <= 0){
				this.isRunning = false;
				game.gameScene.gameOver();
				clearInterval(this.invalId);
			}
		},1000);
	}

	exit(){
		super.exit();
		clearInterval(this.invalId);
	}

	checkNext(){
		let resultArr = this.data.result;
		for(let str of resultArr){
			if(this.resultStr == str){
				this.isRunning = false;
				game.gameEffect.showPassEffect(()=>{
					game.gameScene.gotoNextLevel();
				},this);
				break;
			}
		}
	}

	clkTxt(e){
		if(this.isTweening) return;
		let text = e.target;
		text.isSelected = !text.isSelected;
		if(text.isSelected){
			this.resultStr = this.resultStr == '※' ? '' : this.resultStr;
			this.resultStr += text.text;
			text.interactive = false;
			let tween = PixiTween.slide(text,game.stageCenterX,this.rGrp.y+this.text.y,10);
			this.isTweening = true;
			tween.onComplete = ()=>{
				text.visible = false;
				this.text.text = this.resultStr;
				this.isTweening = false;
				this.checkNext();
			};
			this.hasSelArr.push(text);
		}
	}

	clkResult(){
		if(this.resultStr == '※' || this.isTweening) return;
		this.resultStr = this.resultStr.substr(0,this.resultStr.length-1);
		if(this.resultStr.length == 0){
			this.resultStr = '※';
		}
		this.text.text = this.resultStr;
		let text = this.hasSelArr.pop();
		text.visible = true;
		this.isTweening = true;
		let tween = PixiTween.slide(text,(game.stageWidth - 80)*Math.random(),game.stageCenterY*Math.random(),10);
		tween.onComplete = ()=>{
			PixiTween.removeTween(tween);
			text.isSelected = false;
			text.interactive = true;
			this.text.text = this.resultStr;
			this.isTweening = false;
		};
	}

	createAll(){
		let str = this.data.content;
		let style = {stroke:0xff0000,strokeThickness:2};
		for(let value of str){
			style.fill = Math.ceil(0xffffff*(Math.random()+1)/2);
			style.fontSize = Math.ceil(30*Math.random()) + 48;
			let text = new PIXI.Text(value,style);
			text.x = (game.stageWidth - 200)*Math.random();
			text.y = (3/4)*(game.stageHeight - 80)*Math.random();
			this.gGrp.addChild(text);
			text.interactive = true;
			text.buttonMode = true;
			text.speedx = 2*Math.random();
			text.speedy = 2*Math.random();
			text.isSelected = false;
			text.on('pointerdown',this.clkTxt,this);
		}
	}

	update(){
		if(!this.isRunning) return;
		for(let text of this.gGrp.children){
			if(text.isSelected) continue;
			text.x += text.speedx;
			text.y += text.speedy;
			if(text.x > (game.stageWidth - 80)){
				text.speedx *= -1;
			}
			else if(text.x <= 0){
				text.speedx *= -1;
			}
			if(text.y > (3/4)*(game.stageHeight - 80)){
				text.speedy *= -1; 
			}
			else if(text.y < 0){
				text.speedy *= -1;
			}
		}
	}
}