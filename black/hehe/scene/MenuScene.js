import * as PIXI from '../../libs/pixi.min'
import BaseScene from '../base/BaseScene'
import Button from '../items/Button'
export default class MenuScene extends BaseScene{
	constructor(){
		super();
		this.init();
	}

	init(){
		game.app.renderer.backgroundColor = 0xCDB7B5;
		// let bg = new PIXI.Sprite(PIXI.loader.resources["netImg"].texture);
		// this.addChild(bg);
		this.startBtn = new Button("开始");
		this.startBtn.x = game.stageCenterX - this.startBtn.width/2;
		this.startBtn.y = game.stageCenterY - 150;
		this.addChild(this.startBtn);
		this.startBtn.on("pointerdown",this.clkToStart);

		let rankBtn = new Button("排行榜",7);
		rankBtn.x = game.stageCenterX - this.startBtn.width/2;
		rankBtn.y = this.startBtn.y + 200;
		this.addChild(rankBtn);
		rankBtn.on("pointerdown",this.clkToRank,this);
	}

	clkToStart(){
		game.gameScene.currentLevel = 0;
		game.gameScene.gotoNextLevel();
	}

	clkToRank(){
		game.gameScene.rankView.open();
		// game.gameEffect.showPassEffect();
	}
}