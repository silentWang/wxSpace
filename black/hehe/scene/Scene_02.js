import * as PIXI from '../../libs/pixi.min'
import BaseScene from '../base/BaseScene'
export default class Scene_02 extends BaseScene{
	constructor(data){
		super();
		this.data = data;//{level:1,score:100,speed:15};
		this.init();
	}

	init(){
		game.app.renderer.backgroundColor = 0xBDFCC9;
		//当前屏幕的grid
		this.gridArr = [];
		//当前对象池的grid
		this.poolsArr = [];

		this.gameGrp = new PIXI.Container();
		this.addChild(this.gameGrp);

		this.score = 0;
		this.loseCnt = 0;
		this.desctxt = new PIXI.Text(`第二关 别碰白块(目标分${this.data.score})  您的分数 ${this.score}`,DataUtil.fontBlueStyle);
		this.desctxt.x = 20;
		this.desctxt.y = 20;
		this.addChild(this.desctxt);

		this.speedy = this.data.speed;
	}

	getPools(){
		let graphics;
		for(let grid of this.poolsArr){
			if(grid && !grid.alive){
				graphics = grid;
				break;
			}
		}
		if(!graphics){
			graphics = new PIXI.Graphics();
			this.gameGrp.addChild(graphics);
		}
		let color = Math.ceil(2*Math.random())%2 == 0 ? 0x000000:0xffffff;
		graphics.beginFill(color);
		graphics.lineStyle(2,0xff0000,1);
		graphics.drawRect(0,0,240,320);
		graphics.endFill();
		graphics.y = -graphics.height;
		graphics.alive = true;
		graphics.name = color == 0x000000 ? 'black':'white';
		graphics.visible = true;
		graphics.interactive = true;
		graphics.on('pointerdown',this.clkGrid,this);
		if(!this.poolsArr.includes(graphics)){
			this.poolsArr.push(graphics);
		}
		return graphics;
	}

	enter(){
		super.enter();
		let graphics = this.getPools();
		graphics.visible = true;
		graphics.x = (game.stageWidth/3)*Math.floor(3*Math.random());
		graphics.speedy = this.speedy;
		this.gridArr.push(graphics);
	}

	createGrids(){
		if(!this.isRunning) return;
		let top = this.gridArr[this.gridArr.length - 1];
		let bottom = this.gridArr[0];
		if(top && top.y >= -5){
			let graphics = this.getPools();
			graphics.visible = true;
			graphics.x = (game.stageWidth/3)*Math.floor(3*Math.random());
			graphics.speedy = this.speedy;
			this.gridArr.push(graphics);
		}
		if(bottom && bottom.y >= game.stageHeight){
			bottom = this.gridArr.shift();
			bottom.alive = false;
			bottom.speedy = 0;
			console.log(this.gridArr.length);
			if(bottom.name == 'black'){
				this.loseCnt++;
				if(this.loseCnt >= 3){
					this.isRunning = false;
					game.gameScene.gameOver();
				}
			}
		}
	}

	clkGrid(e){
		if(!this.isRunning) return;
		let grid = e.target;
		if(grid.name == 'black'){
			this.score++;
			grid.visible = false;
			grid.interactive = false;
			grid.name = '';
			// this.gridArr.splice(this.gridArr.indexOf(grid),1);
			this.desctxt.text = `第二关 别碰白块(目标分${this.data.score})  您的分数 ${this.score}`;
			if(this.score >= this.data.score){
				this.isRunning = false;
				game.gameEffect.showPassEffect(()=>{
					game.gameScene.gotoNextLevel();
				});
			}
		}
		else if(grid.name == 'white'){
			this.isRunning = false;
			game.gameScene.gameOver();
		}
	}

	exit(){
		super.exit();
		this.gameGrp.removeChildren();
		this.poolsArr = null;
		this.gridArr = null;
	}

	update(){
		super.update();
		if(!this.isRunning) return;
		for(let grid of this.gridArr){
			grid.y += grid.speedy;
		}
		this.createGrids();
	}
}