import * as PIXI from '../../libs/pixi.min'
import BaseScene from '../base/BaseScene'
export default class Scene_06 extends BaseScene{
	constructor(data){
		super();
		//level 1,2,3 分别代表蜜蜂数量  time:蜜蜂攻击冷却时间 毫秒
		this.data = data;//{level:3,time:500};
		this.init();
	}

	init(){
		let style = new PIXI.TextStyle({
			fill:'#00ff00',
			fontSize:60,
			stroke:'#000000',
			strokeThickness:20,
			fontWeight:'bold'
		});
		this.player = new PIXI.Text('Ж',style);
		this.player.x = game.stageCenterX;
		this.player.y = game.stageHeight - this.player.height;
		this.player.interactive = true;
		this.addChild(this.player);

		this.player.on("pointerdown",this.dragStart,this)
			.on("pointerup",this.dragEnd,this)
			.on("pointerupoutside",this.dragEnd,this)
			.on("pointermove",this.dragMove,this);

		this.bullet = new PIXI.Text('֍',{fill:'#00000f',fontSize:30,stroke:'#ff0000',strokeThickness:2});
		this.bullet.x = this.player.x + (this.player.width - this.bullet.width)/2;
		this.bullet.y = this.player.y - this.bullet.height;
		this.addChild(this.bullet);

		this.createList();
	}

	createList(){
		this.listGrp = new PIXI.Container();
		this.addChild(this.listGrp);
		let style = new PIXI.TextStyle({
			fontSize:60,
		});
		let len = this.getByLevel();
		for(let i = 0;i<len;i++){
			let txt = new PIXI.Text('🐝',style);
			txt.anchor.set(0.5);
			txt.x = 80+80*(i%8);
			txt.y = 40+80*Math.floor(i/8);
			PixiTween.breathe(txt);
			this.listGrp.addChild(txt);
		}
	}

	getByLevel(){
		switch(this.data.level){
			case 1 : return 8*5;
			case 2 : return 8*7;
			case 3 : return 8*10;
			default:return 8*5;
		}
	}

	dragStart(event){
		this.dragData = event.data;
		this.isDragging = true;
		this.doffsetx = this.player.x - this.dragData.global.x;
	}

	dragEnd(){
		this.isDragging = false;
		this.dragData = null;
		if(this.player.x < 0){
			this.player.x = 0;
		}
		if(this.player.x > game.stageWidth - 60){
			this.player.x = game.stageWidth - 60;
		}
	}

	dragMove(){
		if(this.isDragging){
			var newPos = this.dragData.getLocalPosition(this.player.parent);
			this.player.x = newPos.x + this.doffsetx;
			if(this.player.x < 0){
				this.player.x = 0;
			}
			if(this.player.x > game.stageWidth - 60){
				this.player.x = game.stageWidth - 60;
			}
			this.player.y = game.stageHeight - this.player.height;
			this.checkCollision();
		}
	}

	enter(){
		super.enter();
		this.time = this.data.time;
		this.interId = setInterval(()=>{
			if(this.listGrp.children.length <= 0){
				clearInterval(this.interId);
				this.isRunning = false;
				game.gameEffect.showPassEffect(()=>{
					game.gameScene.gotoNextLevel();
				},this);
				return;
			}
			let bee = this.listGrp.getChildAt(Math.floor(this.listGrp.children.length*Math.random()));
			bee.tween = PixiTween.slide(bee,this.player.x + 30,this.player.y + 150);
			bee.tween.onComplete = ()=>{
				bee.visible = false;
				this.listGrp.removeChild(bee);
				bee.tween = null;
			}
		},this.time);
	}

	update(){
		if(!this.isRunning) return;
		this.bullet.y-=10;
		if(this.bullet.y <= 0){
			this.bullet.y = this.player.y - this.bullet.height;
			this.bullet.x = this.player.x + (this.player.width - this.bullet.width)/2;
		}
		this.checkCollision();
	}

	checkCollision(){
		for(let bee of this.listGrp.children){
			if(PixiBump.hit(this.bullet,bee)){
				if(bee.tween){
					PixiTween.removeTween(bee.tween);
					bee.tween = null;
				}
				bee.visible = false;
				this.listGrp.removeChild(bee);
				this.bullet.y = this.player.y - this.bullet.height;
				this.bullet.x = this.player.x + (this.player.width - this.bullet.width)/2;
				return;
			}
			if(PixiBump.hit(this.player,bee)){
				this.player.interactive = false;
				PixiTween.removeTween(bee.tween);
				bee.x = this.player.x + this.player.width/2;
				bee.y = this.player.y + this.player.height/2;
				game.gameScene.gameOver();
				this.isRunning = false;
				clearInterval(this.interId);
				return;
			}
		}
	}

} 