import * as PIXI from '../../libs/pixi.min'
import BaseScene from '../base/BaseScene'
/*乒乓球*/
export default class Scene_04 extends BaseScene{
	constructor(data){
		super();
		this.data = data;//{level:1,score:100,time:60};
		this.init();
	}

	init(){
		game.app.renderer.backgroundColor = 0x000000;
		this.descStr = '乒乓球';
		this.sText = new PIXI.Text(this.descStr.charAt(Math.floor(2*Math.random()+1)));
		this.sText.style = {
			fill:Math.ceil(0xffffff*(Math.random()+1)/2),
			fontSize:160,
			letterSpacing:50,
			stroke:'#ffff00',
			strokeThickness:2,
			fontWeight:'bold'
		}
		this.sText.anchor.set(0.5);
		this.sText.x = game.stageCenterX;
		this.sText.y = game.stageCenterY - 100;
		this.addChild(this.sText);
		this.score = 0;
		this.initTitle();
		this.initResult();
	}

	initTitle(){
		this.tGrp = new PIXI.Container();
		this.addChild(this.tGrp);

		this.levelText = new PIXI.Text(`乒乓球(目标${this.data.score})  分数:${this.score}`,DataUtil.fontWhiteStyle);
		this.levelText.x = 20;
		this.levelText.y = 20;
		this.tGrp.addChild(this.levelText);

		this.timeLeft = new PIXI.Text('剩余时间 00:00',DataUtil.fontGreenStyle);
		this.timeLeft.x = game.stageWidth - this.timeLeft.width - 20;
		this.timeLeft.y = 20;
		this.tGrp.addChild(this.timeLeft);
		this.leftTime = this.data.time;
	}

	initResult(){
		this.bottomGrp = new PIXI.Container();
		this.bottomGrp.y = (3/4)*game.stageHeight;
		this.addChild(this.bottomGrp);
		let graphics = new PIXI.Graphics();
		graphics.beginFill(0x8EE5EE,1);
		graphics.lineStyle(2,0x00ff00,1);
		graphics.drawRect(0,0,game.stageWidth,400);
		graphics.beginFill(0xB3EE3A);
		graphics.drawRect(90,90,120,120);
		graphics.drawRect(300,90,120,120);
		graphics.drawRect(510,90,120,120);
		graphics.endFill();
		this.bottomGrp.addChild(graphics);

		this.rTxt1 = new PIXI.Text('乒');
		this.rTxt1.style = {
			fill:'#0000ff',
			fontSize:80,
			stroke:'#0000ff',
			strokeThickness:1
		}
		this.rTxt1.x = 110;
		this.rTxt1.y = 110;

		this.rTxt2 = new PIXI.Text('乓');
		this.rTxt2.style = {
			fill:'#0000ff',
			fontSize:80,
			stroke:'#0000ff',
			strokeThickness:1
		}
		this.rTxt2.x = 320;
		this.rTxt2.y = 110;

		this.rTxt3 = new PIXI.Text('球');
		this.rTxt3.style = {
			fill:'#0000ff',
			fontSize:80,
			stroke:'#0000ff',
			strokeThickness:1
		}
		this.rTxt3.x = 530;
		this.rTxt3.y = 110;
		this.rTxt1.interactive = true;
		this.rTxt2.interactive = true;
		this.rTxt3.interactive = true;
		this.rTxt1.on('pointerdown',this.clkTxt,this);
		this.rTxt2.on('pointerdown',this.clkTxt,this);
		this.rTxt3.on('pointerdown',this.clkTxt,this);

		this.bottomGrp.addChild(this.rTxt1);
		this.bottomGrp.addChild(this.rTxt2);
		this.bottomGrp.addChild(this.rTxt3);
	}

	clkTxt(e){
		if(!this.isRunning) return;
		let text = e.target;
		if(text instanceof PIXI.Text == false) return;
		let str = this.sText.text;
		this.sText.text = `${str}${text.text}`;
		if(this.checkResult(this.sText.text)){
			this.sText.text = this.descStr.charAt(Math.floor(2*Math.random()+1));
			this.sText.style.fill = Math.ceil(0xffffff*(Math.random()+1)/2);
			let rand = Math.floor(2*Math.random()+1);
			this.rTxt1.text = this.descStr.charAt(rand);
			this.rTxt2.text = this.descStr.charAt(++rand%3);
			this.rTxt3.text = this.descStr.charAt(++rand%3);
			this.score++;
			this.levelText.text = `乒乓球(目标分${this.data.score})  分数:${this.score}`;
			if(this.score >= this.data.score){
				this.isRunning = false;
				game.gameEffect.showPassEffect(()=>{
					game.gameScene.gotoNextLevel();
				},this);
			}
		}
		else{
			this.isRunning = false;
			game.gameScene.gameOver();
		}
	}

	checkResult(str){
		if(this.sText.text == '乒乓') return true;
		if(this.sText.text == '乓球') return true;
		if(this.sText.text == '球乒') return true;
		if(this.sText.text == '乒乓球') return true;
		return false;
	}

	enter(){
		super.enter();
		this.invalId = setInterval(()=>{
			this.leftTime--;
			this.timeLeft.text = DataUtil.getTimeStr(this.leftTime);
			if(this.leftTime <= 0){
				this.isRunning = false;
				game.gameScene.gameOver();
				clearInterval(this.invalId);
			}
		},1000);
	}

	exit(){
		super.exit();
		clearInterval(this.invalId);
	}

}
