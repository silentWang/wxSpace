//2D跳一跳
import * as PIXI from '../../libs/pixi.min'
import BaseScene from '../base/BaseScene'
export default class Scene_09 extends BaseScene{
	constructor(data){
		super();
		this.data = data;
		this.init();
	}

	init(){
		let graphics = new PIXI.Graphics();
		graphics.lineStyle(1,0x000000);
		graphics.beginFill(0x000000);
		graphics.drawRect(0,0,game.stageWidth,game.stageHeight);
		graphics.endFill();
		this.addChild(graphics);
		this.interactive = true;
		this.on('pointerdown',this.downHandler,this);
		this.on('pointerup',this.upHandler,this);

		this.score = 0;
		this.times = this.data.times;
		this.levelText = new PIXI.Text(`${this.data.title}(目标分${this.data.score}) 分数:${this.score}`,DataUtil.fontWhiteStyle);
		this.levelText.x = 20;
		this.levelText.y = 20;
		this.addChild(this.levelText);
		this.timeLeft = new PIXI.Text(`剩余机会:${this.times}`,DataUtil.fontGreenStyle);
		this.timeLeft.x = game.stageWidth - this.timeLeft.width - 20;
		this.timeLeft.y = 20;
		this.addChild(this.timeLeft);

		this.pGx = 20;
		this.pGy = game.stageHeight - 100;
		this.curGrid = this.createGrid();
		this.curGrid.y = this.pGy;
		this.curGrid.x = this.pGx;

		this.tarGrid = this.createGrid();
		this.tarGrid.y = this.pGy;
		this.tarGrid.x = DataUtil.rndmnFloat(200,game.stageWidth - 100);

		this.bText = new PIXI.Text('蹦一蹦',DataUtil.fontEff2Style);
		this.bText.anchor.set(0.5);
		this.bText.x = game.stageCenterX;
		this.bText.y = game.stageCenterY;
		this.bText.alpha = 0.15;
		this.addChild(this.bText);

		let pbw = 10;
		let pbh = 15;
		this.player = new PIXI.Graphics();
		this.player.beginFill(0xCD950C);
		this.player.lineStyle(1,0x00ff00);
		this.player.drawRect(-pbw,-pbh,pbw*2,pbh*2);
		this.player.drawCircle(0,-2*pbh,pbh);
		this.player.drawRect(-2*pbw,-pbh,pbw,pbh*1.5);
		this.player.drawRect(pbw,-pbh,pbw,pbh*1.5);
		this.player.drawRect(-pbw,pbh,pbw,pbh*1.5);
		this.player.drawRect(0,pbh,pbw,pbh*1.5);
		this.player.endFill();
		this.addChild(this.player);
		this.player.x = this.pGx + 50;
		this.player.y = this.pGy - this.player.height/2 + 5;
		PixiTween.breathe(this.player,0.9,0.9);
		this.curTime = 0;
		this.isJump = false;
		this.speedx = 0;
	}

	downHandler(){
		this.curTime = new Date().getTime();
		this.isShowPress = true;
	}

	upHandler(){
		let nowTime = new Date().getTime();
		let time = nowTime - this.curTime;
		this.isShowPress = false;
		if(time <= 0) return;
		let tweens = [[this.player,'rotation',0,2*Math.PI,10]];
		PixiTween.makeTween(tweens);
		this.speedy = -time/50;
		if(this.speedy < -30){
			this.speedy = -30;
		}
		this.speedx = time/100;
		this.isJump = true;
	}

	collision(){
		this.speedy = 0;
		this.player.y = this.tarGrid.y - this.player.height/2 + 5;
		this.isJump = false;
		this.score++;
		this.levelText.text = `${this.data.title}(目标分${this.data.score}) 分数:${this.score}`;
		if(this.score >= this.data.score){
			this.isRunning = false;
			game.gameEffect.showPassEffect(()=>{
				game.gameScene.gotoNextLevel();
			},this);
			return;
		}
		PixiTween.slide(this.player,this.pGx + 50,this.pGy - this.player.height/2 + 5,10);
		let tween = PixiTween.slide(this.tarGrid,this.pGx,this.pGy,10);
		let grid = this.curGrid;
		this.curGrid = this.tarGrid;
		this.tarGrid = grid;
		tween.onComplete = ()=>{
			this.tarGrid.y = -this.tarGrid.height;
			this.tarGrid.x = DataUtil.rndmnFloat(200,game.stageWidth - 100);
			PixiTween.slide(this.tarGrid,this.tarGrid.x,this.pGy,20);
		}
	}

	createGrid(){
		let graphics = new PIXI.Graphics();
		graphics.beginFill(0xCDC1C5);
		graphics.lineStyle(1,0x8B8386);
		graphics.drawRect(0,0,100,100);
		graphics.endFill();
		this.addChild(graphics);
		return graphics;
	}

	exit(){
		super.exit();
		PixiTween.removeTween();

	}

	update(){
		if(!this.isRunning) return;
		if(this.isShowPress){
			this.bText.scale.x -= 0.001;
			this.bText.scale.y -= 0.001;
		}
		else{
			this.bText.scale.set(1);
		}

		if(!this.isJump) return;
		if(PixiBump.hit(this.player,this.tarGrid)){
			this.collision();
			return;
		}
		this.speedx--;
		if(this.speedx <= 0){
			this.speedx = 0;
		}
		this.player.x += this.speedx;
		this.speedy++;
		this.player.y += this.speedy;
		if(this.player.y >= game.stageHeight){
			this.times--;
			this.timeLeft.text = `剩余机会:${this.times}`;
			if(this.times <= 0){
				game.gameScene.gameOver();
			}
			else{
				this.isJump = false;
				this.player.x = this.pGx + 50;
				this.player.y = this.pGy - this.player.height/2 + 5;
			}
		}
	}

}