import * as PIXI from '../../libs/pixi.min'
import BaseScene from '../base/BaseScene'
export default class Scene_08 extends BaseScene{
	constructor(data){
		super();
		this.data = data;//{score:100,speed:5};
		this.init();
	}

	init(){
		let graphics = new PIXI.Graphics();
		graphics.lineStyle(1,0x000000);
		graphics.beginFill(0x000000);
		graphics.drawRect(0,0,game.stageWidth,game.stageHeight);
		graphics.endFill();
		this.addChild(graphics);
		this.houseGrp = new PIXI.Container();
		this.addChild(this.houseGrp);
		this.interactive = true;
		this.on('pointerdown',this.tapScreen,this);

		this.player = new PIXI.Graphics();
		this.player.lineStyle(1,0x00ff00,1);
		this.player.beginFill(0xE3A869);
		this.player.drawRoundedRect(50,25,40,15,10);
		this.player.beginFill(0xCD853F);
		this.player.drawEllipse(30,30,40,25);
		this.player.beginFill(0xffff00);
		this.player.drawCircle(60,14,10);
		this.player.beginFill(0x0000ff);
		this.player.drawCircle(60,14,3);
		this.player.endFill();
		this.player.y = game.stageCenterY - this.player.height/2;
		this.player.x = game.stageCenterX - this.player.width/2;
		this.addChild(this.player);
		this.pSpeed = 0;

		this.score = 0;
		this.levelText = new PIXI.Text(`${this.data.title}(目标分${this.data.score}) 分数:${this.score}`,DataUtil.fontWhiteStyle);
		this.levelText.x = 20;
		this.levelText.y = 20;
		this.addChild(this.levelText);
	}

	tapScreen(){
		if(!this.isStart){
			this.isStart = true;
			this.intervalId = setInterval(()=>{
				let g1 = this.getPools();
				g1.x = game.stageWidth + DataUtil.rndmnFloat(0,120);
				g1.y = DataUtil.rndmnFloat(0,-300);
				let g2 = this.getPools();
				g2.x = game.stageWidth + DataUtil.rndmnFloat(0,120);
				g2.y = DataUtil.rndmnFloat(game.stageHeight - g1.height,game.stageHeight - g1.height + 300);
				this.houseGrp.addChild(g1);
				this.houseGrp.addChild(g2);
			},1500);
		}
		else{
			this.pSpeed = -10;
		}
	}

	getPools(){
		this.pools = !this.pools ? [] : this.pools;
		for(let pg of this.pools){
			if(pg.x <= -pg.width){
				pg.passed = false;
				return pg;
			}
		}
		let graphics = new PIXI.Graphics();
		graphics.lineStyle(5,0xff0000);
		graphics.beginFill(0x00cc00);
		graphics.drawRect(0,0,180,600);
		graphics.endFill();
		this.pools.push(graphics);
		return graphics;
	}

	update(){
		if(!this.isStart || !this.isRunning) return;
		for(let house of this.houseGrp.children){
			house.x -= this.data.speed;
			if(PixiBump.hit(this.player,house)){
				this.reset();
				game.gameScene.gameOver();
			}
			if(!house.passed && this.player.x > house.x + house.width){
				this.score++;
				this.levelText.text = `丑鸟先飞(目标分${this.data.score}) 分数:${this.score}`;
				house.passed = true;
				if(this.score >= this.data.score){
					this.reset();
					game.gameEffect.showPassEffect(()=>{
						game.gameScene.gotoNextLevel();
					},this);
					return;
				}
			}
		}
		this.pSpeed++;
		this.player.y += this.pSpeed;
		if(this.player.y >= game.stageHeight){
			this.reset();
			game.gameScene.gameOver();
		}
	}

	reset(){
		clearInterval(this.intervalId);
		this.isStart = false;
		this.isRunning = false;
	}

}