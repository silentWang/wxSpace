import * as PIXI from '../../libs/pixi.min'
import BaseScene from '../base/BaseScene'

export default class Scene_10 extends BaseScene{
	constructor(data){
		super();
		this.data = data;
		this.init();
	}

	init(){
		let graphics = new PIXI.Graphics();
		graphics.lineStyle(1,0x000000);
		graphics.beginFill(0x000000);
		graphics.drawRect(0,0,game.stageWidth,game.stageHeight);
		graphics.endFill();
		this.addChild(graphics);
		this.interactive = true;
		this.on('pointerdown',this.downHandler,this);
		
		this.score = 0;
		this.levelText = new PIXI.Text(`${this.data.title}(目标分${this.data.score}) 分数:${this.score}`,DataUtil.fontWhiteStyle);
		this.levelText.x = 20;
		this.levelText.y = 20;
		this.addChild(this.levelText);

		this.sGrid = this.getPools();
		this.sGrid.y = 100;
		PixiTween.slide(this.sGrid,game.stageWidth - this.sGrid.width,this.sGrid.y,60,'smoothstep',true);
		this.addChild(this.sGrid);

		this.gGrp = new PIXI.Sprite();
		this.addChild(this.gGrp);

		this.billGrp = new PIXI.Container();
		this.addChild(this.billGrp);
		this.pNames = ['阿牛','比比','郭嘉','韩信','普京','比尔'];
	}

	downHandler(){
		if(this.isDowning) return;
		this.curGrid = this.getPools();
		this.curGrid.x = this.sGrid.x;
		this.curGrid.y = this.sGrid.y;
		this.curGrid.speed = 20;
		this.billGrp.addChild(this.curGrid);
		this.isDowning = true;
	}

	enter(){
		super.enter();
		let billme = this.getBill();
		this.billGrp.addChild(billme);
		this.intervalId = setInterval(()=>{
			let bill = this.getBill();
			this.billGrp.addChild(bill);
		},2000);
	}

	getBill(){
		this.bPools = !this.bPools ? [] : this.bPools;
		for(let bill of this.bPools){
			if(bill.isDead){
				bill.isDead = false;
				bill.speedx = 2*(Math.random()+1);
				bill.x = -100*Math.random();
				bill.y = game.stageHeight - bill.height;
				bill.speedy = -40*Math.random();
				let name0 = this.pNames[Math.floor(this.pNames.length*Math.random())];
				bill.getChildAt(0).text = name0;
				bill.name = name0;
				return bill;
			}
		}

		let graphics = new PIXI.Graphics();
		graphics.beginFill(DataUtil.getSevenColor());
		graphics.lineStyle(1,0x000000);
		let temp = 80 + 50*Math.random();
		graphics.drawCircle(0,0,temp/2);
		graphics.endFill();
		graphics.isDead = false;
		graphics.speedx = 2*(Math.random()+1);
		graphics.x = -100*Math.random();
		graphics.y = game.stageHeight - graphics.height;
		graphics.speedy = -40*Math.random();
		graphics.radius = temp/2;

		let bname = this.pNames[Math.floor(this.pNames.length*Math.random())];
		let text = new PIXI.Text(bname,{fill:0xffffff,fontSize:Math.floor(temp/3),stroke:0x000000,strokeThickness:2});
		text.anchor.set(0.5);
		graphics.addChild(text);
		graphics.name = bname;
		this.bPools.push(graphics);
		return graphics;
	}

	getPools(){
		this.pools = !this.pools ? [] : this.pools;
		for(let grid of this.pools){
			if(grid.isDead){
				grid.isDead = false;
				this.curGrid.visible = true;
				return grid;
			}
		}
		let graphics = new PIXI.Graphics();
		graphics.beginFill(DataUtil.getSevenColor());
		graphics.lineStyle(1,0x000000);
		graphics.drawCircle(0,0,30);
		graphics.endFill();
		graphics.radius = 30;
		this.pools.push(graphics);
		graphics.isDead = false;
		return graphics;
	}

	exit(){
		super.exit();
		clearInterval(this.intervalId);
	}

	update(){
		if(!this.isRunning) return;
		for(let bill of this.billGrp.children){
			if(bill == this.curGrid) continue;
			bill.x += bill.speedx;
			bill.rotation += 0.05;
			bill.speedy+=0.5;
			bill.y += bill.speedy;
			if(bill.y >= game.stageHeight - bill.height){
				bill.y = game.stageHeight - bill.height;
				bill.speedy = 0;
			}
			if(bill.x >= game.stageWidth){
				bill.speedx = 0;
				if(!bill.isDead){
					this.billGrp.removeChild(bill);
				}
				bill.isDead = true;
			}
		}

		if(!this.isDowning) return;
		if(this.curGrid.speed == 0) return;
		this.curGrid.y += this.curGrid.speed;
		if(this.curGrid.y > game.stageHeight){
			this.curGrid.isDead = true;
			this.curGrid.speed = 0;
			this.isDowning = false;
			this.billGrp.removeChild(this.curGrid);
			return;
		}
		
		for(let person of this.billGrp.children){
			if(person == this.curGrid) continue;
			if(PixiBump.circleCollision(this.curGrid,person)){
				this.curGrid.speed = 0;
				this.curGrid.idDead = true;
				this.curGrid.visible = false;
				this.isDowning = false;
				person.speedx = 0;
				this.billGrp.removeChild(person);
				person.isDead = true;
				if(person.name != '比尔'){
					game.gameScene.gameOver();
					this.isRunning = false;
					return;
				}
				else{
					this.score++;
					this.levelText.text = `${this.data.title}(目标分${this.data.score}) 分数:${this.score}`;
					if(this.score >= this.data.score){
						this.isRunning = false;
						game.gameEffect.showPassEffect(()=>{
							game.gameScene.gotoNextLevel();
						},this);
						return;
					}
				}
			}
		}
	}

}