import * as PIXI from '../../libs/pixi.min'
import BaseScene from '../base/BaseScene'

export default class Scene_05 extends BaseScene{
	constructor(data){
		super();
		this.data = data;//{level:1,score:100,time:60};
		this.init();
	}

	init(){
		this.score = 0;
		this.levelText = new PIXI.Text(`打地鼠(目标${this.data.score})  分数:${this.score}`,DataUtil.fontWhiteStyle);
		this.levelText.x = 20;
		this.levelText.y = 20;

		this.timeLeft = new PIXI.Text('剩余时间 01:00',DataUtil.fontGreenStyle);
		this.timeLeft.x = game.stageWidth - this.timeLeft.width - 20;
		this.timeLeft.y = 20;
		this.addChild(this.timeLeft);
		this.leftTime = this.data.time;
		this.addChild(this.levelText);

		this.ratList = [];
		this.createRatList();
	}

	createRatList(){
		let [xx,yy] = [40,100];
		for(let i = 0;i<9;i++){
			let item = new RatItem();
			item.x = xx + (i%3)*220;
			item.y = yy + 220*Math.ceil((i+1)/3);
			item.on('pointerdown',this.tapRat,this);
			this.addChild(item);
			this.ratList.push(item);
		}
	}

	tapRat(e){
		let item = e.target;
		if(!item) return;
		if(item.isShow){
			this.score++;
			this.levelText.text = `打地鼠(目标${this.data.score})  分数:${this.score}`;
			item.isShow = false;
			if(this.score >= this.data.score){
				this.isRunning = false;
				clearInterval(this.invalId);
				game.gameEffect.showPassEffect(()=>{
					game.gameScene.gotoNextLevel();
				},this);
			}
		}
	}

	enter(){
		super.enter();
		this.invalId = setInterval(()=>{
			this.leftTime--;
			this.timeLeft.text = DataUtil.getTimeStr(this.leftTime);
			if(this.leftTime <= 0){
				this.isRunning = false;
				game.gameScene.gameOver();
				clearInterval(this.invalId);
			}
			this.randomShow(this.leftTime);
		},1000);
	}

	exit(){
		super.exit();
		clearInterval(this.invalId);
	}

	randomShow(time){
		this.ratList.sort(()=>0.5-Math.random());
		if(time >= (3/4)*this.data.time){
			this.ratList[0].isShow = true;
		}
		else if(time >= (2/4)*this.data.time){
			this.ratList[0].isShow = true;
			this.ratList[1].isShow = true;
		}
		else if(time >= (1/4)*this.data.time){
			this.ratList[0].isShow = true;
			this.ratList[1].isShow = true;
			this.ratList[2].isShow = true;
		}
		else if(time >= 0){
			this.ratList[0].isShow = true;
			this.ratList[1].isShow = true;
			this.ratList[2].isShow = true;
			this.ratList[3].isShow = true;
		}
	}
}


/*鼠的item*/
class RatItem extends PIXI.Sprite{
	constructor(){
		super();
		this.init();
	}

	init(){
		let graphics = new PIXI.Graphics();
		graphics.lineStyle(1,0xCDB7B5,1);
		graphics.beginFill(0xA0522D);
		graphics.drawCircle(100,100,100);
		graphics.endFill();
		this.addChild(graphics);

		let style = new PIXI.TextStyle({
			fill:'#DEB887',
			fontSize:120,
			stroke:'8E8E8E',
			strokeThickness:20,
			fontWeight:'bold'
		});
		this.ratTxt = new PIXI.Text('癙',style);
		this.ratTxt.x = 26;
		this.ratTxt.y = 20;
		this.addChild(this.ratTxt);

		this.ratTxt.visible = false;
		this._isShow = false;
		this.interactive = true;
		this.buttonMode = true;
	}

	get isShow(){
		return this._isShow;
	}

	set isShow(value){
		this._isShow = value;
		if(value === true){
			this.ratTxt.visible = true;
			let idx = setTimeout(()=>{
				this.ratTxt.visible = false;
				this._isShow = false;
				clearTimeout(idx);
			},1000);
		}
		else{
			this.ratTxt.visible = false;
		}
	}

}