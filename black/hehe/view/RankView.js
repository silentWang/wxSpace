import * as PIXI from '../../libs/pixi.min'
import BaseView from '../base/BaseView'
import Button from '../items/Button'
export default class RankView extends BaseView{
	constructor(){
		super();
		this.init();
	}

	init(){
		this.rankUI = new PIXI.Sprite();
		let bg = new PIXI.Sprite(PIXI.loader.resources["./res/commbg.png"].texture);
		bg.scale.set(4,6);
		this.rankUI.x = game.stageCenterX - bg.width/2;
		this.rankUI.y = game.stageCenterY - bg.height/2;
		this.rankUI.addChild(bg);
		let title = new PIXI.Text('好友排行榜',DataUtil.fontTitleStyle);
		title.x = (bg.width - title.width)/2;
		title.y = 40;
		this.rankUI.addChild(title);
		this.createList();
		this.addChild(this.rankUI);
		this.rankUI.interactive = true;
		this.rankUI.on('pointerdown',()=>{this.close()});
		this.addChild(this.rankUI);
		this.createList();
	}

	createList(){
		let text0 = new PIXI.Text(`姓名　　　排名　　　分数`,DataUtil.fontTwoTitleStyle);
		text0.x = 40;
		text0.y = 100;
		this.rankUI.addChild(text0);
		for(let i = 0;i<10;i++){
			let str = `戚薇　　　　${i+1}　　　160000`;
			let text = new PIXI.Text(str,DataUtil.fontWhiteStyle);
			text.x = 40;
			text.y = (i+1)*50+100;
			this.rankUI.addChild(text);
		}
		this.myRankTxt = new PIXI.Text(`我的排行：1`,DataUtil.fontRedStyle);
		this.myRankTxt.x = 80;
		this.myRankTxt.y = 11*50+130;
		this.rankUI.addChild(this.myRankTxt);
	}
}