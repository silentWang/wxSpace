import * as PIXI from '../libs/pixi.min'
export default class GameLoader{
	constructor(){
		this.init();
	}

	init(){
		//images
		PIXI.loader.add([
			"./res/loadingbg.png",
			"./res/loadingbar.png",
			"./res/cloudbg.jpg",
			"./res/commbg.png",
		]);
		// PIXI.loader.add('netImg',`${DataUtil.baseResUrl}images/mario.jpg`);
		// PIXI.loader.add('ballAnim',`${DataUtil.baseResUrl}images/ball.json`);
		// PIXI.loader.add([
		// 	'./res/sound/homeBg.mp3',
		// ]);
		PIXI.loader.load();
		PIXI.loader.onProgress.add(this.loadProgress,this);
		PIXI.loader.onComplete.add(this.loadComplete,this);
	}

	initLoader(){
		this.loadGrp = new PIXI.Sprite();
		this.loadingbg = new PIXI.Sprite(PIXI.loader.resources["./res/loadingbg.png"].texture);
		this.loadingbar = new PIXI.Sprite(PIXI.loader.resources["./res/loadingbar.png"].texture);
		this.loadingbg.anchor.set(0.5);
		this.loadingbar.anchor.set(0,0.5);
		this.loadingbar.width = 0;
		this.loadingbg.x = game.stageCenterX;
		this.loadingbg.y = game.stageCenterY;
		this.loadingbar.x = this.loadingbg.x - this.loadingbg.width/2;
		this.loadingbar.y = game.stageCenterY;
		this.loadGrp.addChild(this.loadingbg);
		this.loadGrp.addChild(this.loadingbar);
		game.app.stage.addChild(this.loadGrp);
	}

	loadProgress(loader,resource){
		console.log("loading...");
		if(resource.url == "./res/loadingbar.png"){
			this.initLoader();
		}
		if(!this.loadGrp) return;
		this.loadingbar.width = this.loadingbg.width*loader.progress/100;
	}

	loadComplete(){
		console.log("complete");
		this.loadGrp.visible = false;
		game.init();
	}
}