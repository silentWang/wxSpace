import './libs/weapp-adapter'
import * as PIXI from './libs/pixi.min'
import Charm from './libs/Charm'
import Bump from './libs/Bump'
import './utils/DataUtil'
import GameConfig from './hehe/GameConfig'
import GameLoader from './hehe/GameLoader'
import GameScene from './hehe/GameScene'
import GameEffect from './hehe/GameEffect'

const { pixelRatio, windowWidth, windowHeight } = wx.getSystemInfoSync();
/*这个是为了兼容事件的处理*/
PIXI.interaction.InteractionManager.prototype.mapPositionToPoint = (point,x,y)=>{
  point.x = x*pixelRatio;
  point.y = y*pixelRatio;
}
//缓动库
window.PixiTween = new Charm();
//2d 碰撞检测库
window.PixiBump = new Bump();

export default class Main{
	constructor(){
		this.app = new PIXI.Application({
			width:windowWidth*pixelRatio,
			height:windowHeight*pixelRatio,
			view:canvas
		});
		this.stageWidth = this.app.renderer.width;
		this.stageHeight = this.app.renderer.height;
		this.stageCenterX = windowWidth*pixelRatio/2;
		this.stageCenterY = windowHeight*pixelRatio/2;
		this.gameLoader = new GameLoader();
		this.app.ticker.add(this.loop);
	}

	init(){
		this.gameConfig = new GameConfig();
		this.gameScene = new GameScene();
		this.gameEffect = new GameEffect();
		this.gameScene.enterScene('menu');
	}

	loop(){
		PixiTween.update();
	}
}